# mnfrontend: A simple frontend for Simple TODO Application

## Compiling

### With Docker

- go to project folder ( cd /path/to/mnfrontend )
- docker build -t mnfrontend .
- docker run -p 127.0.0.1:3000:80/tcp mnfrontend
- go to http://127.0.0.1:3000 and check ok

### With NPM

#### Npm Development Mode

- go to project folder ( cd /path/to/mnfrontend )
- npm install
- npm start
- go to http://127.0.0.1:3000 and check ok

#### Npm Production

- go to project folder ( cd /path/to/mnfrontend )
- npm install
- npm run build
- serve -s build
- go to http://127.0.0.1:3000 and check ok

## Deployment

### Pipeline

IF you want use pipeline for build or deploy edit bitbucket-pipelines.yml or ...gitlab-ci.yml

## Features

- List Todos
- Add new todo item
    
## TODO LIST

- Delete a todo item
- Modify a todo item
