import axios from 'axios';

class TodoApiClient {
    private host: string;
    
    constructor(host: string) {
        this.host = host;
    }

    async listTodos() {
        let response = await axios.get(this.host + "/todo");
        console.log("list todo response", response.data)
        if (response.data.success === false) {
            return null
        }
        if (response.data.result === null) {
            response.data.result = [];
        }
        return response.data.result;
    }

    async addTodo(todo: Todo) {
        console.log("post todo request",this.host + "/todo", todo)
        let response = await axios.post(this.host + "/todo", todo);
        console.log("post todo response", response.data)
        if (response.data.success === false) {
            return null
        }
        return response.data.result;
    }
}

export interface Todo {
    key?: number;
    issue: string;
}

export default TodoApiClient;
