import TodoApiClient from "./TodoApiClient";

const path = require("path")
const chai = require("chai")
const { Pact } = require("@pact-foundation/pact")
const chaiAsPromised = require("chai-as-promised")
const expect = chai.expect

chai.use(chaiAsPromised)

const provider = new Pact({
    port: 3003,
    cors: true,
    log: path.resolve(process.cwd(), "logs", "pact.log"),
    dir: path.resolve(process.cwd(), "pacts"),
    consumer: "mnfrontend",
    provider: "todo-api",
    logLevel: "DEBUG" // the usual log levels - DEBUG, INFO, WARN, ERROR, etc.
});

describe("TodoApi Client", () => {
    const todo = {
        issue: 'Biraz süt al',
    };
    const expectedCreateResponse = {
        success: true,
        error: '',
        result: {
            key: Number(1),
            issue: 'Biraz süt al'
        }
    };
    const expectedListResponse = {
        success: true,
        error: '',
        result: [{"issue": "Biraz süt al", "key": 1}]
    };

    beforeAll(async () => {
        jest.setTimeout(1000);
        await provider.setup();

    }, 15000); // wait for 15s max if the jest hook needs more than 5s for pact
    afterAll(() => provider.finalize())
    afterEach(() => provider.verify())

    describe("when some request is made", () => {
        beforeEach(async () => {

            await provider.addInteraction({
                uponReceiving: 'a request to create a new todo',
                withRequest: {
                    method: 'POST',
                    path: '/todo',
                    body: todo,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
                    body: expectedCreateResponse
                }
            });

            await provider.addInteraction({
                uponReceiving: 'a request to list todos',
                withRequest: {
                    method: 'GET',
                    path: '/todo',
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
                    body: expectedListResponse
                }
            });
        });

        const apiClient = new TodoApiClient("http://localhost:3003");

        it('should list todo resource', async () => {
            const result = await apiClient.listTodos();
            expect(result).to.be.a("array")
        });

        it('should create a todo resource', async () => {
            const result = await apiClient.addTodo(todo);
            expect(result).to.be.a("object")
        });
    });
});
