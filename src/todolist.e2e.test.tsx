import puppeteer from "puppeteer";

describe("TodoList.tsx", () => {
    let browser: puppeteer.Browser;
    let page: puppeteer.Page;

    beforeAll(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
    });

    it("iş listesi", async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector(".chakra-badge");
        const text = await page.$eval(".chakra-badge", (e) => e.textContent);
        expect(text).toContain("Yapılacaklar listesi boş");
    });

    afterAll(() => browser.close());
});