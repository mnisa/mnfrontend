import {Button, HStack, Input, useToast} from '@chakra-ui/react';
import React, {useState} from 'react';
import {Todo} from "./service/TodoApiClient";

interface AddTodoProps {
    addTodo: any
}

export const AddTodo = (props: AddTodoProps) => {

    const toast = useToast();

    const handleSubmit = (e: any) => {
        e.preventDefault();
        if (!content) {
            toast({
                title: 'No content',
                status: 'error',
                duration: 2000,
                isClosable: true,
            });
            return;
        }

        const todo: Todo = {
            issue: content,
        };

        props.addTodo(todo);
        setContent('');
    }

    const [content, setContent] = useState('');

    return (
        <form onSubmit={handleSubmit}>
            <HStack mt='8'>
                <Input
                    variant='filled'
                    placeholder='yeni iş ekle'
                    value={content}
                    aria-label="todo-input"
                    onChange={(e) => setContent(e.target.value)}
                />
                <Button className="submit-btn" colorScheme='blue' px='8' type='submit'>Ekle</Button>
            </HStack>
        </form>
    );
}

export default AddTodo;