/**
 * @jest-environment jsdom
 */

import {cleanup, fireEvent} from "@testing-library/react";
import React from "react";
import AddTodo from "./AddTodo";
import {render} from "./test-utils";

afterEach(cleanup);

const defaultProps = {
    addTodo: jest.fn(),
};

test('renders yapılacak iş text input', () => {
    const addTodo = jest.fn();
    const app = render(<AddTodo {...defaultProps} addTodo={addTodo}/>);
    const issueTextElement = app.getByPlaceholderText("yeni iş ekle");
    expect(issueTextElement).toBeInTheDocument();
});

test('renders kaydet submit button', () => {
    const addTodo = jest.fn();
    const app = render(<AddTodo {...defaultProps} addTodo={addTodo}/>);
    const saveButtonElement = app.getByText("Ekle");
    expect(saveButtonElement).toBeInTheDocument();
});

test('calls correct function on submit', () => {
    const addTodo = jest.fn();
    const {getByText, getByPlaceholderText} = render(<AddTodo {...defaultProps} addTodo={addTodo}/>)
    const input = getByPlaceholderText("yeni iş ekle");
    fireEvent.change(input, {target: {value: "biraz süt al"}});
    fireEvent.click(getByText('Ekle'));
    expect(addTodo).toHaveBeenCalled();
});