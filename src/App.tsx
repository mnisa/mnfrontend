import * as React from "react"
import {
    ChakraProvider,
    Box,
    VStack,
    Grid,
    theme,
    Heading,
} from "@chakra-ui/react"
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import TodoApiClient, {Todo} from "./service/TodoApiClient";

const apiBaseUrl: string = (process.env.REACT_APP_TODO_API_BASEURL as string);

export const App = () => {

    const [todos, setTodos] = React.useState<Todo[]>([]);

    React.useEffect(() => {
        let apiClient = new TodoApiClient(apiBaseUrl);
        const fetchData = async () => {
            let response = await apiClient.listTodos();
            if (response === null) {
                alert('iş listesi alınamadı!')
                return
            }
            setTodos(response);
        };
        fetchData();
    }, []);

    function addTodo(todo: Todo) {
        let apiClient = new TodoApiClient(apiBaseUrl);
        let response = apiClient.addTodo(todo);
        if (response === null) {
            alert('iş eklenemedi!')
            return
        }
        setTodos([...todos, todo]);
    }

    return (
        <ChakraProvider theme={theme}>
            <Box textAlign="center" fontSize="xl">
                <Grid minH="100vh" p={3}>
                    <VStack spacing={8}>
                        <VStack p={4}>
                            <Heading className={'header'} mb='8' fontWeight='extrabold' size='xl'>Basit Todo Uygulaması</Heading>
                            <AddTodo addTodo={addTodo}/>
                            <TodoList todos={todos}/>
                        </VStack>
                    </VStack>
                </Grid>
            </Box>
        </ChakraProvider>
    )
}
