import puppeteer from "puppeteer";

describe("App.tsx", () => {
    let browser: puppeteer.Browser;
    let page: puppeteer.Page;

    beforeAll(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
    });

    it("açılış sayfası header var mı", async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector(".header");
        const text = await page.$eval(".header", (e) => e.textContent);
        expect(text).toContain("Basit Todo Uygulaması");
    });

    afterAll(() => browser.close());
});