/**
 * @jest-environment jsdom
 */

import {cleanup} from "@testing-library/react";
import React from "react";
import TodoList from "./TodoList";
import {render} from "./test-utils";

afterEach(cleanup);

const defaultProps = {
    todos: [{key: Number(1234), issue: 'biraz süt al'}],
};

test('display no todos if there is nothing todo', () => {
    const {queryByText} = render(<TodoList {...defaultProps} todos={[]}/>);
    expect(queryByText("Yapılacaklar listesi boş")).toBeTruthy();
});

test('todo list render correctly', () => {
    const {queryByText} = render(<TodoList {...defaultProps} />);
    expect(queryByText("biraz süt al")).toBeTruthy();
});
