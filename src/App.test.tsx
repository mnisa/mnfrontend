/**
 * @jest-environment jsdom
 */

import React from "react"
import {render} from "./test-utils"
import {App} from "./App"

test('renders todo header', () => {
    const app = render(<App/>);
    const todoHeaderElement = app.getByText("Basit Todo Uygulaması");
    expect(todoHeaderElement).toBeInTheDocument();
});

