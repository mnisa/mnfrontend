import puppeteer from "puppeteer";

describe("AddTodo.tsx", () => {
    let browser: puppeteer.Browser;
    let page: puppeteer.Page;

    beforeAll(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
    });

    it("yeni iş ekleme", async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector(".chakra-input");
        await page.focus(".chakra-input");
        await page.keyboard.type( "biraz süt al");
        await page.click(".submit-btn");
        await page.waitForSelector('p.chakra-text');
        const text = await page.$eval("p.chakra-text", (e) => e.textContent);
        expect(text).toContain("biraz süt al");
    });

    afterAll(() => browser.close());
});