import React from 'react';
import {
    HStack,
    VStack,
    Text,
    StackDivider,
    Badge,
} from '@chakra-ui/react';
import {Todo} from './service/TodoApiClient';

interface TodoListProps {
    todos: Todo[]
}

export const TodoList = (props: TodoListProps) => {
    if (!props.todos.length) {
        return (
            <Badge colorScheme='green' p='4' m='4' borderRadius='lg'>
                Yapılacaklar listesi boş
            </Badge>
        );
    }

    return (
        <VStack
            divider={<StackDivider/>}
            borderColor='gray.100'
            borderWidth='1px'
            p='2'
            borderRadius='lg'
            w='100%'
            maxW={{base: '90vw', sm: '80vw', lg: '50vw', xl: '40vw'}}
            alignItems='stretch'
        >
            {props.todos.map((todo: Todo) => (
                <HStack key={String(todo.key)}>
                    <Text>{todo.issue}</Text>
                </HStack>
            ))}
        </VStack>
    );
}

export default TodoList;